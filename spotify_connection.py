import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

auth_manager = SpotifyClientCredentials()
sp = spotipy.Spotify(auth_manager=auth_manager)


def get_playlists_by_user(username: str) -> list:
    def fetch_playlists(username: str, playlists: list = None, limit=50) -> list:

        if playlists is None:
            playlists = list()

        batch = sp.user_playlists(username, limit=limit, offset=len(playlists))["items"]
        new_playlists = playlists + batch

        if len(batch) < limit:
            return new_playlists

        return fetch_playlists(username, new_playlists)

    return fetch_playlists(username)


def get_playlist_info(playlist_uri: str) -> dict:
    # extract id from uri and remove query parameters
    playlist_id = playlist_uri.split('/')[-1].split('?')[0]
    return sp.playlist(playlist_id)
